const movies = [
	{
		title: "The Lord of the Rings Trilogy",
		genre: "action",
		releasedDate: Date(2001,12,10),
		rating:3.5,
		displayRating:function(){
			console.log(`The movie ${this.title} has ${this.rating} stars.`)
		}

	},

	{
		title: "The Shawshank Redemption",
		genre: "drama",
		releasedDate: new Date(1994,9,10),
		rating:2.5,
		displayRating:function(){
			console.log(`The movie ${this.title} has ${this.rating} stars.`)
		}
	},

	{
		title: "Life is Beautiful",
		genre: "drama",
		releasedDate: new Date(1997,12,20),
		rating:2.4,
		displayRating:function(){
			console.log(`The movie ${this.title} has ${this.rating} stars.`)
		}
	},

	{
		title: "Jesus Christ Superstar",
		genre: "musical",
		releasedDate: new Date(1973,6,26),
		rating:4.5,
		displayRating:function(){
			console.log(`The movie ${this.title} has ${this.rating} stars.`)
		}
	},

	{
		title: "Bruce Almighty", //
		genre: "comedy",
		releasedDate: new Date(2003,5,14),
		rating:4.3,
		displayRating:function(){
			console.log(`The movie ${this.title} has ${this.rating} stars.`)
		}
	},
	{
		displayRating2: function(findRating){
			let i = 0;
			let j = 0;
			console.log(`Movies with ${findRating} and above ratings:`)
			do{
				if(movies[i].rating>=findRating){
					console.log(`${movies[i].title} ${movies[i].rating} stars.`)
					j+=1;
				}
				i++;
			}
			while(i < movies.length-1)

			if (j==0){
					console.log(`No movies with that rating.`)
				}

		}
	}

]

//sir alan's
// function showAllMovies(){
// 	for(let i=0; i < movies.length; i++){
// 		console.log(movies[i].title + " " + movies[i].genre)
// 	}
// }

let showAllMovies = () => {
	for(let i=0; i < (movies.length-1); i++){
		console.log(`${movies[i].title} ${movies[i].genre}`)
	}
}

let showTitles = findRating => movies[5].displayRating2(findRating);

/* homework
Write a function called showTitles 
that accepts a single 
numeric parameter

Example:
showTitles(4)

Output
Movies with 4 and above ratings
1) Exorcist 4 stars
2) Godfather 5 stars

No movies with that rating
*/